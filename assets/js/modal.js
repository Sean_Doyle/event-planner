
var modal;
var userInfo;
var header;
var submit;

function setup(args){
	modal = document.getElementById("modal-innerWrapper");
	modal.innerHTML = ""; 

	var exit = document.createElement("a");
	exit.setAttribute("href","#close");
	exit.setAttribute("title","Close");
	exit.setAttribute("class","close");
	exit.innerHTML = "X";
	modal.appendChild(exit);

	header = document.createElement("h2");
	
	userInfo = document.createElement("p");
	userInfo.innerHTML = "Username: <br />";

	var username = document.createElement("input");
	username.setAttribute("type","text");
	username.setAttribute("id","unInput");
	userInfo.appendChild(username);

	userInfo.innerHTML = userInfo.innerHTML + "<br /> Password: <br />";
	var password = document.createElement("input");
	password.setAttribute("type","password");
	password.setAttribute("id","pwInput");
	userInfo.appendChild(password);

	if(args == "signUp"){
		userInfo.innerHTML = userInfo.innerHTML + "<br /> Email: <br />"
		var email = document.createElement("input");
		email.setAttribute("type","text");
		email.setAttribute("id","emailInput");
		userInfo.appendChild(email);
	}

	submit = document.createElement("button");
	submit.innerHTML = "Submit";
}


function logInDialog(){
	location.href="#openModal";
	setup("logIn");

	header.innerHTML = "Login <hr>";
	modal.appendChild(header);

	submit.setAttribute("onclick","logIn()");

	modal.appendChild(userInfo);
	modal.appendChild(submit);
}

function signUpDialog(){
	location.href="#openModal";
	setup("signUp");

	header.innerHTML = "Sign Up <hr>";
	modal.appendChild(header);

	submit.setAttribute("onclick","signUp()");
	
	modal.appendChild(userInfo);
	modal.appendChild(submit);
}

