/**
 * @author Seán Doyle
 * @date 19/10/15
 */

function logIn(){
	var username = document.getElementById("unInput").value;
	var password = document.getElementById("pwInput").value;

	if(!username || !password ){  
		//TODO => better feedback on bad data.
		alert("Please fill in all fields available!");
	}else{

		/**
		 * By hashing  the password on the client side it protects the users 
		 * password in the event the packet is intercepted when sent over the network.
		 */
		var pwHash = md5(password); 

		$.ajax({
			type: 'POST',
			url: 'serverRequests.php',
			data: { 'logIn':"logIn", 'username':username, 'password':pwHash },
			success: function(data){
				location.href = "main.php";
			}
		});
	}
}

function signUp(){
	var username = document.getElementById("unInput").value;
	var password = document.getElementById("pwInput").value;
	var email = document.getElementById("emailInput").value;

	if(!username || !password || !email){  
		//TODO => better feedback on bad data.
		alert("Please fill in all fields available!");
	}else{

		/**
		 * By hashing  the password on the client side it protects the users 
		 * password in the event the packet is intercepted when sent over the network.
		 */
		var pwHash = md5(password); 

		$.ajax({
			type: 'POST',
			url: 'serverRequests.php',
			data: { 'signUp':"signUp", 'username':username, 'password':pwHash, 'email':email },
			success: function(data){
				location.href = "main.php";
			}
		});
	}
}

function logout(){
	$.ajax({
		type: 'GET',
		url: 'serverRequests.php',
		data: { 'Logout': "true" },
		success: function(data){
			console.log("Successfully Logged Out.");
			location.reload();
		}
	})
}