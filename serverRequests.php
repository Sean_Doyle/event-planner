<?php
	session_start();

	$server = "localhost";
	$user = "root";
	$pass = "";
	$db = "event_planner";

	$conn = new mysqli($server, $user, $pass, $db) or die("Failed to connect to mySQL Server");

	if(isset($_POST['signUp'])){
		$username = $_POST['username'];

		//Cant have duplicate usernames
		if(checkUsername($username)){
			die("Username has already been taken");
		}
		
		$password = $_POST['password'];
		$email = $_POST['email'];
		
		signUp($username, $password, $email);
	}

	if(isset($_POST['logIn'])){
		$username = $_POST['username'];
		$password = $_POST['password'];

		logIn($username, $password);
	}

	if(isset($_GET['Logout'])){
		session_destroy();
	}

	/**
	 *	Creates a new user and inserts them into the pending_user table to wait on admin approval.
	 */
	function signUp($username, $password, $email){
		global $conn;

		$id = getID("users") +1;
		$salt = openssl_random_pseudo_bytes(32, $cstrong);
		$hashedPW = md5($password . $salt);

		$moderator = 0;
		$approved = 0;

		$stmt = $conn->prepare("INSERT INTO users(id,name,password,salt,email,isModerator,isApproved) VALUES(?,?,?,?,?,?,?)");
		$stmt->bind_param("issssii",$id,$username,$hashedPW,$salt,$email,$moderator,$approved);
		$stmt->execute();

		$_SESSION['Current_User'] = $username;
		echo "Successfully registered as: " . $_SESSION['Current_User'];
	}

	/**
	 *	Retrieves stored salt + pw from the specified user and compares the stored pw with a hash of the submited pw with the salt
	 */
	function logIn($username, $password){
		global $conn;

		$stmt = $conn->prepare("SELECT password, salt, isModerator, isApproved FROM users WHERE name=?");
		$stmt->bind_param("s",$username);
		$stmt->execute();

		$stmt->bind_result($storedPW,$salt,$isModerator,$isApproved);
		$stmt->fetch();
		
		$hashedPW = md5($password . $salt);

		if(strcmp($hashedPW, $storedPW) == 0){
			$_SESSION['Current_User'] = $username;
			$_SESSION['modStatus'] = $isModerator;
			$_SESSION['registeredStatus'] = $isApproved;
			echo "Logged in as: " . $_SESSION['Current_User'] . "\nYour moderator status is: " . $isModerator . "\nYour registered member status is: " . $isApproved;	
		} else {
			echo "Incorrect Username or Password";
		}
	}

	/**
	 *	Gets max id from specified table.
	 */ 
	function getID($table){
		global $conn;

		$stmt = $conn->prepare("SELECT MAX(id) FROM " . $table);
		$stmt->execute();

		$stmt->bind_result($id);
		$stmt->fetch();

		return $id;
	}

	/**
	 *	Checks if a username already exists as we cannot have duplicate usernames.
	 */
	function checkUsername($username){
		global $conn;

		$stmt = $conn->prepare("SELECT name FROM users WHERE name=?");
		$stmt->bind_param("s",$username);
		$stmt->execute();

		$stmt->bind_result($result);
		$stmt->fetch();

		$isDuplicate = !empty($result);
		return $isDuplicate;
	}
?>