<?php
	session_start();
?>
<html>
	<head>
		<title>Event-Planner</title>

		<link rel="stylesheet" href="assets/css/modal.css">
	</head>
	<body>
		<div id="wrapper">

			<?php if(!isset($_SESSION['Current_User'])){ ?>

				<div id="loggedOut">
					<button id="signUpBtn" onclick="signUpDialog()">SignUp</button>
					<button id="logInBtn" onclick="logInDialog()">Login</button>
				</div>

			<?php } elseif(isset($_SESSION['Current_User'])){ ?>

				<p> You are logged in as: <?php echo $_SESSION['Current_User'] ?> </p>

			<?php } else { ?>

				<p> Issue in Current_User session </p>

			<?php } ?>
			
		</div>
		<div id="openModal" class="modalDialog">
			<div id="modal-innerWrapper">
				<a href="#close" title="Close" class="close">X</a>
		</div>
	</div>
	</body>
	<footer>
		<script type="text/javascript" src="lib/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="lib/md5.min.js"></script>
		<script type="text/javascript" src="assets/js/main.js"></script>
		<script type="text/javascript" src="assets/js/modal.js"></script>
	</footer>
</html>
