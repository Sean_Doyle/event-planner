# Event Planner #

### Description ###

This planner allows the creation of "events" by users who are given sufficient permissions.  
These events in turn can be signed up to by other users to show whether they will be attending
said event.