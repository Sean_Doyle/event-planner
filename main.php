<?php
	session_start();

	if(!isset($_SESSION['Current_User'])){
		header("Location: index.php");
		die();
	}	
?>
<html>
	<head>

	</head>
	<body>
		<div> 
			Logged in as: <?php echo "" . $_SESSION['Current_User'] ?> 
			<button id="logout" onclick="logout()">Sign Off</button><hr>
		</div>

		<div id="events">

			<?php if($_SESSION['modStatus'] == 1){ ?>
				<button id="createEventBtn" onclick="createEvent()">Create an Event</button>
				<button id="memberManagamentBtn" onclick="memberManagement()">Manage Users</button>
			<?php } ?>

			<p>
				<a href="#">Sample Event 1</a> : Oct 3rd 21:30 <br />
				<a href="#">Sample Event 2</a> : Oct 16th 15:45 <br />
				<a href="#">Sample Event 3</a> : Oct 29th 19:00 <br />
			</p>
		</div>
	</body>
	<footer>
		<script type="text/javascript" src="lib/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="assets/js/main.js"></script>
	</footer>
</html>